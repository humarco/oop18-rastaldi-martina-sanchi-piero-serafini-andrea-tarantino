package utilities;

/**
 * Enum representing the possible types of elements.
 * Andrea Serafini.
 *
 */
public enum Elements {
    /**
     * The hero, the main good character, controlled by a player.
     */
    EROE,
    /**
     * The minotaurus, the evil character, can be controlled by every player.
     */
    MINOTAURO,
    /**
     * The wall, a moving part of the maze.
     */
    MURO;
}
